package at.upstream_mobility.itacademy.bored.get;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions.*;
import org.springframework.web.reactive.function.client.WebClient;

import javax.swing.event.ListDataEvent;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class BoredAPIClientTest {


    @Test
    void get_shouldReturnActivity() {

        WebClient webClient = WebClient.builder().build();

            BoredAPIClient client = new BoredAPIClient(webClient);
            String activity = client.get("");
            assertNotNull(activity);

    }

    @Test
    void get_shouldReturnActivityByType(){

    List<String> types = List.of("education","recreational", "social", "diy", "charity",
            "cooking", "relaxation", "music", "busywork");

        for (String type : types){

        WebClient webClient = WebClient.builder().build();
        BoredAPIClient client = new BoredAPIClient(webClient);
        String activityByType = client.get(type);

            assertNotNull(activityByType);

        }



    }
}