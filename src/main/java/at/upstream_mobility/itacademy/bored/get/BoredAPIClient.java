package at.upstream_mobility.itacademy.bored.get;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@ShellComponent
@AllArgsConstructor
public class BoredAPIClient {


    private WebClient webClient;
    private static final URI BASE_URL = URI.create("https://www.boredapi.com/api/activity");


    @ShellMethod("Get a suggestion for an activity of a specified type")
    public String get(@ShellOption(defaultValue = "") String type) {

        URI url = UriComponentsBuilder.fromHttpUrl(BASE_URL.toString())
                .queryParam("type", type)
                .build()
                .toUri();

        ResponseEntity<Activity> activity = webClient
                .get()
                .uri(!type.isEmpty() ? url : BASE_URL)
                .retrieve()
                .toEntity(Activity.class)
                .block();

        return activity.getBody().getActivity() != null ? activity.getBody().getActivity() : activity.getBody().getError();
    }

}
