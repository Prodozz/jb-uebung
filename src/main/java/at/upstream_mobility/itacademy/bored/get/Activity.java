package at.upstream_mobility.itacademy.bored.get;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Activity {

    String activity;
    String type;
    int participants;
    int price;
    String link;
    String key;
    float accessibility;
    String error;
}
